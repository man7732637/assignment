import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fibonacci Numbers',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const FibonacciScreen(),
    );
  }
}

class FibonacciScreen extends StatefulWidget {
  const FibonacciScreen({Key? key}) : super(key: key);

  @override
  _FibonacciScreenState createState() => _FibonacciScreenState();
}

class _FibonacciScreenState extends State<FibonacciScreen> {
  List<Map<String, dynamic>> fibonacciSeries = [];
  List<Map<String, dynamic>> fibonacciBottomSheetCircle = [];
  List<Map<String, dynamic>> fibonacciBottomSheetSquare = [];
  List<Map<String, dynamic>> fibonacciBottomSheetClose = [];
  late ScrollController _scrollControllerFullPage;
  late ScrollController scrollControllerBottomSheet;
  int focusNumber = -1;
  final String iconCheck = "";

  @override
  void initState() {
    super.initState();
    _scrollControllerFullPage = ScrollController();
    scrollControllerBottomSheet = ScrollController();
    generateFibonacciSeries(41);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _scrollControllerFullPage.dispose();
    scrollControllerBottomSheet.dispose();
    super.dispose();
  }

  void scrollToIndex({required int index, required ScrollController scrollController}) {
    double offset = 40.0 * index; // Assuming each item has a height of 50.0 (adjust as needed)

    // Scroll to the calculated offset
    scrollController.animateTo(
      offset,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  void generateFibonacciSeries(int n) {
    int first = 0;
    int second = 1;

    for (int i = 0; i < n; i++) {
      fibonacciSeries.add(
        {
          'index': i,
          'number': first,
          'iconType': first % 3 == 1
              ? "crop_square"
              : first % 3 == 0
              ? "circle"
              : "close"
        },
      );
      int next = first + second;
      first = second;
      second = next;
    }
  }

  void addFibonacciToBottomSheet(Map<String, dynamic> number) {
    {
      if (number['iconType'] == 'circle') {
        fibonacciBottomSheetCircle.add(
          {
            'index': number['index'],
            'number': number['number'],
            'iconType': number['iconType'],
          },
        );
        fibonacciSeries.removeWhere((item) {
          return item['index'] == number['index'];
        });
      } else if (number['iconType'] == 'close') {
        fibonacciBottomSheetClose.add(
          {
            'index': number['index'],
            'number': number['number'],
            'iconType': number['iconType'],
          },
        );
        fibonacciSeries.removeWhere((item) {
          return item['index'] == number['index'];
        });
      } else if (number['iconType'] == 'crop_square') {
        fibonacciBottomSheetSquare.add(
          {
            'index': number['index'],
            'number': number['number'],
            'iconType': number['iconType'],
          },
        );
        fibonacciSeries.removeWhere((item) {
          return item['index'] == number['index'];
        });
      }
      setState(() {});
      buildShowModalBottomSheet(context, number);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mobile Assignment'),
      ),
      body: Center(
        child: ListView(
          controller: _scrollControllerFullPage,
          children: <Widget>[
            const SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: fibonacciSeries.map((number) {
                return ListTile(
                  tileColor: number['index'] == focusNumber ? Colors.red : null,
                  title: Text(
                    "index: " + number['index'].toString() + ", Number: " + number['number'].toString(),
                    style: const TextStyle(
                      fontSize: 16,
                    ),
                  ),
                  trailing: number['iconType'] == "circle"
                      ? const Icon(Icons.circle)
                      : number['iconType'] == "close"
                      ? const Icon(Icons.close)
                      : const Icon(Icons.crop_square),
                  onTap: () {
                    addFibonacciToBottomSheet(number);
                  },
                );
              }).toList(),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> buildShowModalBottomSheet(BuildContext context, Map<String, dynamic> number) {
    return showModalBottomSheet<void>(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
      ),
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(builder: (context, snapshot) {
          WidgetsBinding.instance!.addPostFrameCallback((_) {
            scrollControllerBottomSheet.position.maxScrollExtent;
          });
          return Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
            height: 300,
            child: Container(
              margin: const EdgeInsets.all(8.0),
              child: ListView(
                controller: scrollControllerBottomSheet,
                children: number['iconType'] == "circle"
                    ? fibonacciBottomSheetCircle.map(
                      (e) {
                    return buildListFibonacciBottomSheet(
                      e: e,
                      context: context,
                      number: number,
                      fibonacciBottomSheetIconType: fibonacciBottomSheetCircle,
                      iConType: 'circle',
                    );
                  },
                ).toList()
                    : number['iconType'] == "close"
                    ? fibonacciBottomSheetClose.map(
                      (e) {
                    return buildListFibonacciBottomSheet(
                      e: e,
                      context: context,
                      number: number,
                      fibonacciBottomSheetIconType: fibonacciBottomSheetClose,
                      iConType: 'close',
                    );
                  },
                ).toList()
                    : fibonacciBottomSheetSquare.map(
                      (e) {
                    return buildListFibonacciBottomSheet(
                      e: e,
                      context: context,
                      number: number,
                      fibonacciBottomSheetIconType: fibonacciBottomSheetSquare,
                      iConType: 'crop_square',
                    );
                  },
                ).toList(),
              ),
            ),
          );
        });
      },
    );
  }

  Widget buildListFibonacciBottomSheet({
    required Map<String, dynamic> e,
    required BuildContext context,
    required Map<String, dynamic> number,
    required List<Map<String, dynamic>> fibonacciBottomSheetIconType,
    required String iConType,
  }) {
    return ListTile(
      onTap: () {
        fibonacciSeries.add(
          {
            'index': e['index'],
            'number': e['number'],
            'iconType': iConType,
          },
        );
        fibonacciSeries.sort((a, b) => a['index']!.compareTo(b['index']!));
        fibonacciBottomSheetIconType.removeWhere((item) {
          return item['index'] == e['index'];
        });
        scrollToIndex(index: e['index'], scrollController: _scrollControllerFullPage);

        Navigator.of(context).pop();
        setState(() {
          focusNumber = e['index'];
        });
      },
      tileColor: number['index'] == e['index'] ? Colors.green : null,
      title: Text(
        'Number: ${e['number']}',
        style: const TextStyle(
          fontSize: 18,
        ),
      ),
      subtitle: Text('Index: ${e['index']}'),
      trailing: number['iconType'] == "circle"
          ? const Icon(Icons.circle)
          : number['iconType'] == "close"
          ? const Icon(Icons.close)
          : const Icon(Icons.crop_square),
    );
  }
}
